<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "pxa_isotope"
 *
 * Auto generated by Extension Builder 2014-01-31
 *
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Isotope',
	'description' => 'Isotope-extension ',
	'category' => 'plugin',
	'author' => 'Andreas Olsson',
	'author_email' => 'andreas@pixelant.se',
	'author_company' => 'Pixelant',
	'shy' => '',
	'priority' => '',
	'module' => '',
	'state' => 'alpha',
	'internal' => '',
	'uploadfolder' => '0',
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'version' => '0.0.2',
	'constraints' => array(
		'depends' => array(
			'extbase' => '6.0',
			'fluid' => '6.0',
			'typo3' => '6.0',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);

?>
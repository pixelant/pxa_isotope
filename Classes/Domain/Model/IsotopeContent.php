<?php
namespace TYPO3\PxaIsotope\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Andreas Olsson <andreas@pixelant.se>, Pixelant
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package pxa_isotope
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class IsotopeContent extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {
	
	/**
	 * @var integer
	 */
	protected $categories;

	/**
	 * Get creation date
	 *
	 * @return integer
	 */
	public function getCategories() {
		return $this->categories;
	}

	/**
	 * Set creation date
	 *
	 * @param integer $categories
	 * @return void
	 */
	public function setCategories($categories) {
		$this->categories = $categories;
	}

}
?>
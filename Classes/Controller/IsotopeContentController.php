<?php
namespace TYPO3\PxaIsotope\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Andreas Olsson <andreas@pixelant.se>, Pixelant
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package pxa_isotope
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class IsotopeContentController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * isotopeContentRepository
	 *
	 * @var \TYPO3\PxaIsotope\Domain\Repository\IsotopeContentRepository
	 * @inject
	 */
	protected $isotopeContentRepository;	

	/* $GLOBALS['TYPO3_DB']*/


	/**
	 * action list
	 *
	 * @return void
	 */
	
	public function listAction() {
		$isotopeContents = $this->isotopeContentRepository->findAll();

		$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Tx_Extbase_Object_ObjectManager');
		$categoryRepository = $objectManager->get('TYPO3\\CMS\\Extbase\\Domain\\Repository\\CategoryRepository');

		$categorySelection = explode(',', $this->settings['categories']);
		foreach ($categorySelection as $categoryCurrent) {
			$category = $categoryRepository->findByUid($categoryCurrent);
			if ( is_object($category) ) {
				$categories[$categoryCurrent] = strtolower($category->getTitle());	
			}
		}

		$this->view->assign('categories',$categories);
		$this->view->assign('transition',$this->settings['transitionMode']);
		$this->view->assign('layoutMode',$this->settings['layoutMode']);

		$content = array();
		$cObj = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Frontend\\ContentObject\\ContentObjectRenderer');
		/*rendering tt_content items*/
		for ($i=0; $i < count($isotopeContents); $i++) { 
			$elementCats = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('uid_local','sys_category_record_mm','uid_foreign='.$isotopeContents[$i]->getUid());	
			$cat = '';
			foreach ($elementCats as $elementCat) {
				$cat .= strtolower(str_replace(' ', '-', $categories[$elementCat['uid_local']]) . ' ');
			}
			$ttContentConfig = array(
				'tables' => 'tt_content',
				'source' => $isotopeContents[$i]->getUid(),
				'dontCheckPid' => 1
			);
			$content[$i]['content'] = $cObj->RECORDS($ttContentConfig);
			$content[$i]['categories'] = $cat;
		}/*rendering tt_content items*/

//	
		$this->view->assign('isotopeContents', $content);
	}

}
?>
# [pxa_isotope](https://bitbucket.org/pixelant/pxa_isotope)
> TYPO3 extension for Flexible & Animated Layouts of content. It can reveal & hide items with filtering and re–order items with sorting. What content to display is configured in extension by page and categories.

**0.0.2** - (24.10.2014)

* Fix - If a category was deleted in TYPO3, an exception would be thrown when trying to render a page containing isotope content with the deleted category selected.

**0.0.1** - (05.05.2014)

* Initial version
